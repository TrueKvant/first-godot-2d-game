extends Area2D

@export var speed = 400.0
signal hit

var screen_size = Vector2.ZERO

func _ready():
	screen_size = get_viewport_rect().size
	hide()
	

func _process(delta):
	
	# managing inputs
	# there is no positive and negative axis as in unity
	var direction = Vector2.ZERO
	if Input.is_action_pressed("move_right"):
		direction.x += 1
	if Input.is_action_pressed("move_left"):
		direction.x -= 1
	
	if Input.is_action_pressed("move_up"):
		direction.y -= 1
	if Input.is_action_pressed("move_down"):
		direction.y += 1
	
	if direction.length() > 0:
		direction = direction.normalized()
		$AnimatedSprite2D.play()
	else:
		$AnimatedSprite2D.stop()
		
	position += direction * speed * delta
	
	# naive bounding
	position.x = clamp(position.x, 0, screen_size.x)
	position.y = clamp(position.y, 0, screen_size.y)
	
	if direction.y !=0:
		$AnimatedSprite2D.animation = "up"
		$AnimatedSprite2D.flip_v = direction.y > 0
	elif direction.x != 0:
		$AnimatedSprite2D.animation = "right"
		$AnimatedSprite2D.flip_h = direction.x < 0
		$AnimatedSprite2D.flip_v = false


func start(new_position):
	position = new_position
	show()
	$CollisionShape2D.disabled = false


func _on_body_entered(body):
	hide()
	# wait for the physics calculation
	$CollisionShape2D.set_deferred("disabled", true)
	emit_signal("hit")
